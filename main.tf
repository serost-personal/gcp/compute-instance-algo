# Credentials JSON file should be in the GOOGLE_APPLICATION_CREDENTIALS
# JSON is associated with service account

# You can login into service account using
# gcloud auth activate-service-account --key-file=$HOME/key-file.json

terraform {
  # make sure versioning in enabled
  backend "gcs" {
    bucket = "serost-compute-terraform-state"
    prefix = "/remote-state" # save tfstate files in the remote-state dir
  }
}

variable "gcp_project" {}
variable "gcp_region" {}

provider "google" {
  project = var.gcp_project
  region  = var.gcp_region
}
