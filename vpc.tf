# enable serviceusage api also, to allow service accounts to enable api
resource "google_project_service" "compute" {
  project = var.gcp_project
  service = "compute.googleapis.com"
}

# vpc in GCP is global object as opposed to AWS, where it belongs to some region
# subnets are bound specific region
resource "google_compute_network" "main" {
  name = "main"

  # if GLOBAL - one subnet in one region may communicate with subnet in another region
  # if REGIONAL - subnet is restricted to communicate to within the same region
  routing_mode = "REGIONAL"

  auto_create_subnetworks         = false
  delete_default_routes_on_create = true

  mtu = 1460

  depends_on = [google_project_service.compute]
}
